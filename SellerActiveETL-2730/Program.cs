﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NLog;
using SellerActiveETL;
using SellerActiveETL.Enums;
using SellerActiveETL.ETL;
using SellerActiveETL.ETLJobs;
using SellerActiveETL.Rules;

namespace SellerActiveETL_2730
{
    internal class Program
    {
        private static readonly Extract Extract = new Extract();
        private static readonly Transform Transform = new Transform();
        private static readonly Load Load = new Load();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            //Console.WriteLine("Press ENTER to continue...");
            //Console.ReadLine();
            Logger.Trace("Application Startup");
            // Setup
            var job = GetJob();

            if (!job.Run())
                Logger.Error("Job failed.");

            //Console.WriteLine("Press ENTER to continue...");
            //Console.ReadLine();
            Logger.Trace("Application End");
        }

        private static EtlJob GetJob()
        {
            // Configure the source for the job
            var updateInventoryItemMaps = new List<InventoryItemMap>
            {
                new InventoryItemMap {PropertyName = "SKU", ColumnLetter = "C"},
                new InventoryItemMap {PropertyName = "Quantity", ColumnLetter = "H"},
                new InventoryItemMap {PropertyName = "VendorName", ColumnLetter = "A"}
            };
            var source = new Source("order.ekeystone.com", FileType.Csv, SourceType.ImplicitFtpOverTls, true, updateInventoryItemMaps)
            {
                UserName = "S116478",
                Password = "psi64odc",
                FileLocation = "/",
                SourceFileName = "InventoryFile116478.ZIP",
                IsZip = true,
                ZipInternalFileName = "InventoryFile116478.csv",
                UpdateProductSiteMaps = new List<ProductSiteMap>
                {
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "DefaultPrice", ColumnLetter = "E"},
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "FloorPrice", ColumnLetter = "E"},
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "CeilingPrice", ColumnLetter = "E"}
                },
                InsertInventoryItemMaps = new List<InventoryItemMap>
                {
                    new InventoryItemMap {PropertyName = "SKU", ColumnLetter = "C"},
                    new InventoryItemMap {PropertyName = "ProductID", ColumnLetter = "K"},
                    new InventoryItemMap {PropertyName = "Quantity", ColumnLetter = "H"},
                    new InventoryItemMap {PropertyName = "VendorName", ColumnLetter = "A"},
                    new InventoryItemMap {PropertyName = "Title", ColumnLetter = "D"},
                    new InventoryItemMap {PropertyName = "ProductType", Value = "UPC"},
                    new InventoryItemMap {PropertyName = "Condition", Value = "New"},
                },
                InsertProductSiteMaps = new List<ProductSiteMap>
                {
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "DefaultPrice", ColumnLetter = "E"},
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "FloorPrice", ColumnLetter = "E"},
                    new ProductSiteMap {Site = ProductSites.Amazon, PropertyName = "CeilingPrice", ColumnLetter = "E"}
                },
            };


            // Configure the client for the job
            var rules = new List<IRule>
            {
                new InventoryItemStringPropertyFilterRule("SKU", "="),
                new InventoryItemStringPropertyFilterRule("SKU", "\""),
                new InventoryItemStringPropertyFilterRule("ProductID", "="),
                new InventoryItemStringPropertyFilterRule("ProductID", "\""),
                new InventoryItemModifyPriceRule(ProductSites.Amazon, floorModifier: 1.18, floorModifierType: ModifierType.Divide, ceilingModifier: .65, ceilingModifierType: ModifierType.Divide),
                new RequiredPropertyForInsertRule("ProductID")
            };
            var client = new Client("2730", "HFYK01SYGHHU79V1JBHJYWZBPZRI8S49C0H7UOT19EGVU1PUPCXS2ANVQW5JJK1Y",source, rules);

            // Create the job
            return  new EtlJob(client);
        }
    }
}

