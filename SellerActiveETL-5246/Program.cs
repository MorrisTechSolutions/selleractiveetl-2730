﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net.Sockets;
using NLog;
using SellerActiveETL.Enums;
using SellerActiveETL.ETL;
using SellerActiveETL.ETLJobs;
using SellerActiveETL.Rules;
using SellerActiveETL.Services;

namespace SellerActiveETL_5246
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // TODO: Get from config file
        private static string _endPoint = "http://asp.wheelpros.com/ProductPriceDownload/DownloadFile.ashx?custNo=1026566&productselect=Wheels&fileformat=CSV&location=TotalQty";
        private static string _clientId = "5246";
        private static string _apiKey = "8ENWGC4JLB90UHQ1873W47A78569GJH2FZ1RIDQJM5L684NJ4AZTZGFADRYLG5DC";

        private static void Main(string[] args)
        {
            //Console.WriteLine("Press ENTER to begin...");
            //Console.ReadLine();
            Logger.Trace("Application Startup");
            
            // Setup
            var job = GetJob();

            // Run
            if (!job.Run())
                Logger.Error("Job failed.");


            // ***************************************************
            //                    Testing
            // ***************************************************
            /*
            var transform = new Transform();
            job.LocalDataFileName = "5246-20160304T15.58.15.txt";
            var results = transform.Execute(job);
            Logger.Info($"Total Transformed: {results.Item.Count}");

            var sai = new InventoryService(job.Client);
            var itemsFound = 0;
            var itemsNotFound = 0;
            foreach (var item in results.Item.Select(i => i.InventoryItem).ToList().Skip(11400).Take(1000))
            {
                var getResults = sai.RetrieveAsync(item.SKU).Result;
                if (getResults.IsSuccess)
                {
                    itemsFound++;
                    Logger.Info($"SKU {item.SKU} ({getResults.Item.Count}): {getResults.Item.FirstOrDefault()}");
                }
                else itemsNotFound++;
            }
            Console.WriteLine($"Items found    : {itemsFound}");
            Console.WriteLine($"Items not found: {itemsNotFound}");
            */
            // ***************************************************

            //Console.WriteLine("Press ENTER to end...");
            //Console.ReadLine();

            Logger.Trace("Application End");
        }

        private static EtlJob GetJob()
        {
            var updateInventoryItemMaps = new List<InventoryItemMap>
            {
                new InventoryItemMap {PropertyName = "SKU", ColumnLetter = "D"},
                new InventoryItemMap {PropertyName = "Quantity", ColumnLetter = "K"},
            };

            // Configure the source for the job
            var source = new Source(_endPoint, FileType.Csv, SourceType.Http, true, updateInventoryItemMaps);


            // Configure the client for the job
            var rules = new List<IRule>
            {
                new QuantityAdjustmentRule(item => (item.Quantity < 5) && (item.Quantity > 0), ModifierType.Replace, 0),
                new QuantityAdjustmentRule(item => item.Quantity > 10, ModifierType.Replace, 10)
            };

            var client = new Client(_clientId, _apiKey, source, rules);

            // Create the job
            return new EtlJob(client);
        }
    }
}

