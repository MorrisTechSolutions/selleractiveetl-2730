﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PersistentConfig
{
    public class Config<T> where T : IEquatable<T>
    {
        private string _fileName;
        private string FileName
        {
            get { return _fileName ?? (_fileName = typeof (T).Name + ".json"); }
        }

        public bool Upsert(T item) 
        {
            var items = GetItems();

            var index = items.FindIndex(i => i.Equals(item));
            if (index == -1) items.Add(item);
            else items[index] = item;


            var json = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

            File.WriteAllText(FileName,json);


            return true;
        }

        public List<T> GetItems()
        {
            return File.Exists(FileName) ? JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(FileName), new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All}) : new List<T>();
        }
    }
}
