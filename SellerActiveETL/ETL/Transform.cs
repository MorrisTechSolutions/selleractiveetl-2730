﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using NLog;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.ETL
{
    public class Transform
    {
        private readonly Stopwatch _watch = new Stopwatch();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="etlJob"></param>
        /// <returns></returns>
        public Results<List<InventoryItemWrapper>> Execute(EtlJob etlJob)
        {
            _watch.Reset();
            _watch.Start();
            etlJob.CurrentStep = Steps.TransformStart;
            var results = new Results<List<InventoryItemWrapper>>();

            var worker = GetWorker(etlJob);
            if (worker == null)
            {
                Logger.Fatal("Worker is null - return results and aborting transform");
                results.IsSuccess = false;
                results.ErrorCode = "Transform Worker";
                results.ErrorMessage = "A transform worker was not loaded. Aborting transform process.";
                return results;
            }

            var inventoryItems = worker.DoTransform(etlJob.LocalDataFileName, etlJob.Client.Source);

            etlJob.CurrentStep = Steps.TransformProcessingRules;

            ProcessRules(etlJob, inventoryItems);


            // Set results to success
            results.IsSuccess = true;
            results.Item = inventoryItems;

            // Transform done - remove file. 
            try
            {
                if (File.Exists(etlJob.LocalDataFileName)) File.Delete(etlJob.LocalDataFileName);
            }
            catch (Exception ex)
            {
                Logger.Error($"Exception trying to remove source file:{etlJob.LocalDataFileName}");
                Logger.Error(ex.Message);
            }

            etlJob.CurrentStep = Steps.TransformEnd;
            _watch.Stop();
            Logger.Info($"Elapsed time for Transform: {_watch.FormattedElapsed()}");

            return results;
        }

        private static ITransformWorker GetWorker(EtlJob etlJob)
        {
            switch (etlJob.Client.Source.FileType)
            {
                case FileType.Csv:
                    return new CsvTransformWorker();

                case FileType.Unknown:
                    Logger.Fatal("FileType must be set.");
                    return null;
                default:
                    Logger.Fatal($"Unknown FileType: {etlJob.Client.Source.FileType}");
                    return null;
            }
        }

        private static void ProcessRules(EtlJob etlJob, List<InventoryItemWrapper> inventoryItems)
        {
            Logger.Trace("Start");
            var itemCount = inventoryItems.Count;

            var rules = etlJob.Client.Rules.Where(r => r.RuleType == RuleType.PostTransform).ToList();

            Logger.Info($"Processing the following rules on {itemCount:n0} items:");
            foreach (var rule in rules)
                Logger.Info($"     {rule.RuleName}");

            foreach (var item in inventoryItems)
            {
                foreach (var rule in rules)
                    rule.Execute(item.InventoryItem, etlJob);
            }
            Logger.Info("All rules processed.");
            Logger.Trace("End");
        }
    }


    public class CsvTransformWorker : ITransformWorker
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private Source _source;
        public List<InventoryItemWrapper> DoTransform(string sourceFile, Source source)
        {
            Logger.Trace("Start");

            var linesProcessed = 0;
            var skippedLineCount = 0;
            var totalLines = File.ReadLines(sourceFile).Count();
            var runningTotal = 0;
            var tenPercent = (int) totalLines*.10;
            var inventoryItems = new List<InventoryItemWrapper>();
            _source = source;
            Logger.Info($"Parsing CSV file. Number of lines: {totalLines:N0}");

            using (var parser = new TextFieldParser(sourceFile) { TextFieldType = FieldType.Delimited, HasFieldsEnclosedInQuotes = true })
            {
                parser.SetDelimiters(",");

                // Skip first line if there is a header
                if (!parser.EndOfData && source.HasHeader) parser.ReadLine();
                while (!parser.EndOfData)
                {
                    try
                    {
                        if (runningTotal++ > tenPercent)
                        {
                            runningTotal = 0;
                            Logger.Info($"Lines Processed: {linesProcessed:n0}, Skipped Lines: {skippedLineCount:n0}");
                        }

                        // Read fields from file & validate we got something
                        var fields = parser.ReadFields();
                        if (fields == null)
                        {
                            Logger.Debug("fields is null - unable to transform");
                            throw new Exception("fields is null - unable to transform");
                        }

                        // Create the InventoryItem from the fields read & add to list
                        var item = CreateInventoryItem(fields);
                        inventoryItems.Add(new InventoryItemWrapper(item));
                        Logger.Trace($"New Item Created (SKU={item.SKU}), Total Items Processed: {++linesProcessed:N0}");
                    }
                    catch (MalformedLineException mle)
                    {
                        skippedLineCount++;
                        Logger.Debug("MalformedLineException: {0}", mle.Message);
                    }
                    catch (Exception ex)
                    {
                        skippedLineCount++;
                        // Catching all exceptions so we can "skip" a bad line and not stop the whole file from processing.
                        Logger.Debug("{0}: Line {1} {2}", ex.GetType().FullName, parser.LineNumber - 1, ex.Message);
                    }

#if DEBUG
                    // This can be used in debug build to limit how many total records are transformed. 
                    // ReSharper disable once RedundantLogicalConditionalExpressionOperand
                    if (Constants.TESTING_MAX_RECORDS_TO_PROCESS <= 0 || linesProcessed < Constants.TESTING_MAX_RECORDS_TO_PROCESS) continue;
                    Logger.Info($"TESTING MODE: Stopping processing - reached {linesProcessed}");
                    break;
#endif
                }
            }

            Logger.Info("Transformed record count: {0:N0}", inventoryItems.Count);
            Logger.Info("Skipped line count: {0:N0}", skippedLineCount);

            Logger.Trace("End");
            
            return inventoryItems;

        }

        /// <summary>
        /// Using the Update & Insert mappings set in the source this will create a new InventoryItem
        /// with the properties populated from the fields passed in. 
        /// </summary>
        /// <param name="fields">the soure of data for the new inventory item</param>
        /// <returns>A new InventoryItem with all properties populated from the fields.</returns>
        private InventoryItem CreateInventoryItem(IReadOnlyList<string> fields)
        {
            Logger.Trace("Start");
            var item = new InventoryItem();

            // Loop through each Upate Map for the Inventory Items and set the property value from the fields
            _source.UpdateInventoryItemMaps?.ForEach(map => SetPropertyValue(item, map, fields));

            // If There are any Update or Insert ProductSite maps  then initi the ProductSites property. 
            item.ProductSites = _source.UpdateProductSiteMaps.HasAny() || _source.InsertProductSiteMaps.HasAny() ? new List<ProductSite>() : null;

            // Loop through each Upate Map for the ProductSites and set the property value from the fields
            _source.UpdateProductSiteMaps?.ForEach(map => { SetPropertyValue(item.GetProductSite(map.Site), map, fields); });

            // If UpdateInvenotryItemMaps is not null then filter out all properties already mapped 
            _source.InsertInventoryItemMaps?.Where(map => _source.UpdateInventoryItemMaps == null || !_source.UpdateInventoryItemMaps.IsAny(uMap => uMap.PropertyName == map.PropertyName))
                .ToList().ForEach(map => SetPropertyValue(item, map, fields));

            // If UpdateProductSiteMaps is not null then filter out all properties already mapped 
            _source.InsertProductSiteMaps?.Where(map => _source.UpdateProductSiteMaps == null || !_source.UpdateProductSiteMaps.IsAny(uMap => uMap.PropertyName == map.PropertyName))
                .ToList().ForEach(map => { SetPropertyValue(item.GetProductSite(map.Site), map, fields); });

            Logger.Trace("End");
            
            return item;
        }

        /// <summary>
        /// Will set the property parameters value from the fields parameter based on the map parameter.
        /// </summary>
        /// <param name="property">The property to set the value on.</param>
        /// <param name="map">The map used to find the value in the fields parameter</param>
        /// <param name="fields">The source for the value which will be set in the property.</param>
        private static void SetPropertyValue(Object property, IMap map, IReadOnlyList<string> fields)
        {
            Logger.Trace("Start");
        
            // Get the PropertyInfo
            var propertyInfo = property.GetType().GetProperty(map.PropertyName);

            // Determine the value to set based upon the map.
            object value = !String.IsNullOrEmpty(map.Value) ? map.Value : fields[(map.ColumnNumber.HasValue()) ? Convert.ToInt32(map.ColumnNumber) : map.ColumnLetter.ColumnNameToIndex()];

            // Set the property's value - The value will be converted to the proper type.
            propertyInfo.SetValue(property, value == null ? null : Convert.ChangeType(value, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType));

            Logger.Trace("End");
        }
    }

    public interface ITransformWorker
    {
        List<InventoryItemWrapper> DoTransform(string sourceFile, Source source);
    }
}
