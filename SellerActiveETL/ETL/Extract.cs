﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AlexPilotti.FTPS.Client;
using NLog;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.ETL
{
    public class Extract
    {
        private string _endPointUrl;
        private EtlJob _etlJob;
        private Source _source;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private Stopwatch _watch = new Stopwatch();

        public bool Execute(EtlJob etlJob)
        {
            Logger.Trace("Start");
            _watch.Reset();
            _watch.Start();
            etlJob.CurrentStep = Steps.ExtractStart;

            try
            {
                _etlJob = etlJob;
                _source = etlJob.Client.Source;

                _endPointUrl = etlJob.Client.Source.Endpoint;

                Logger.Info("Starting file download...");
                _etlJob.LocalDataFileName = StartDownload();
                Logger.Info("File download finished.");

                // No file name was returned - extract failed.
                if (String.IsNullOrEmpty(_etlJob.LocalDataFileName))
                {
                    Logger.Fatal("No file name was returned - extract failed.");
                    return false;
                }

                _etlJob.CurrentStep = Steps.ExtractEnd;
            }
            catch (Exception e)
            {
                Logger.Fatal($"Error during extract: {e.Message}");
                return false;
            }

            _watch.Stop();
            Logger.Info($"Elapsed time for Extract: {_watch.FormattedElapsed()}");
            Logger.Trace("End");
            return true;
        }
        private string StartDownload()
        {
            switch (_source.SourceType)
            {
                case SourceType.Http: return HttpFileDownload().Result;
                case SourceType.ImplicitFtpOverTls: return ImplicitFtpOverTlsDownload();
                default: return null;
            }
        }

        private string ImplicitFtpOverTlsDownload()
        {
            Logger.Trace("Start");

            try
            {
                if (File.Exists(_source.SourceFileName)) File.Delete(_source.SourceFileName);

                using (var client = new FTPSClient())
                {
                    var hostName = _source.Endpoint;
                    var credential = new NetworkCredential(_source.UserName, _source.Password);
                    client.Connect(hostName, credential, ESSLSupportMode.Implicit, UserValidateServerCertificate);
                    client.GetFile(_source.SourceFileName, _source.SourceFileName, TransferCallback);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal($"There was an error downloading the file: {ex.Message}");
                return null;
            }
            if (_source.IsZip)
            {
                ExtractInternalFileFromZip();
            }
            
            Logger.Trace("End");
            return _source.ZipInternalFileName ?? _source.SourceFileName;
        }

        public void ExtractInternalFileFromZip()
        {
            Logger.Trace("Start");
            try
            {
                var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                if (string.IsNullOrEmpty(path))
                {
                    Logger.Fatal("The path was empty. ");
                    throw new Exception("Path cannot be empty ");
                }

                var zipPath = Path.Combine(path, _source.SourceFileName);
                var extractFileName = Path.Combine(path, _source.ZipInternalFileName);

                // Remove the extracted file if it already exists
                if (File.Exists(extractFileName)) File.Delete(extractFileName);

                using (var archive = ZipFile.OpenRead(zipPath))
                {
                    var entry = archive.Entries.FirstOrDefault(e => e.FullName.Equals(_source.ZipInternalFileName, StringComparison.OrdinalIgnoreCase));
                    if (entry == null) throw new Exception("Unable to find internal file in Zip: " + _source.ZipInternalFileName);
                    entry.ExtractToFile(extractFileName);
                }

                // Remove Zip file
                File.Delete(zipPath);

            }
            catch (Exception ex)
            {
                Logger.Fatal(ex.Message);
                throw;
            }
            Logger.Trace("End");
        }

        private static bool UserValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            return (sslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None;
        }

        /// <summary>
        /// Download a CSV file via HTTP
        /// </summary>
        /// <returns>The name of the file that was downloaded</returns>
        private async Task<string> HttpFileDownload()
        {
            Logger.Trace("Start");
            var fileNamePrefix = _etlJob.Client.ClientId;
            var dts = DateTime.Now.ToString("s").Replace("-", "").Replace(":", ".");
            var fileName = $"{fileNamePrefix}-{dts}.txt";


            using (var client = new HttpClient())
            using (var response = await client.GetAsync(_endPointUrl))
            using (var content = response.Content)
            using (var writer = File.CreateText(fileName))
            {
                await writer.WriteAsync(await content.ReadAsStringAsync());
            }
            Logger.Trace("End: fileName = {0}", fileName);

            return fileName;
        }

        #region Show Download Status for FTP file

        private static readonly Stopwatch Watch = new Stopwatch();

        private static void TransferCallback(FTPSClient sender, ETransferActions action, string localObjectName, string remoteObjectName, ulong fileTransmittedBytes, ulong? fileTransferSize, ref bool cancel)
        {
            switch (action)
            {
                case ETransferActions.FileDownloaded:
                case ETransferActions.FileUploaded:
                    OnFileTransferCompleted(fileTransmittedBytes, fileTransferSize);
                    break;
                case ETransferActions.FileDownloadingStatus:
                case ETransferActions.FileUploadingStatus:
                    OnFileTransferStatus(action, localObjectName, remoteObjectName, fileTransmittedBytes, fileTransferSize);
                    break;
                case ETransferActions.RemoteDirectoryCreated:
                    Logger.Info("Remote directory created: " + remoteObjectName);
                    break;
                case ETransferActions.LocalDirectoryCreated:
                    Logger.Info("Local directory created: " + localObjectName);
                    break;
            }
        }

        private static void OnFileTransferStatus(ETransferActions action, string localObjectName, string remoteObjectName, ulong fileTransmittedBytes, ulong? fileTransferSize)
        {
            if (fileTransmittedBytes != 0) return; // Don't post status update
            // Download / upload start

            Watch.Reset();
            Watch.Start();

            if (action == ETransferActions.FileDownloadingStatus)
            {
                Logger.Info("Source (remote): " + remoteObjectName);
                Logger.Info("Dest (local): " + localObjectName);
            }
            else
            {
                Logger.Info("Source (local): " + localObjectName);
                Logger.Info("Dest (remote): " + remoteObjectName);
            }

            Logger.Info(fileTransferSize != null ? $"File Size: {fileTransferSize.Value.ToString("N0")} Bytes" : "File Size: Unknown");
        }

        private static void OnFileTransferCompleted(ulong fileTransmittedBytes, ulong? fileTransferSize)
        {
            Watch.Stop();

            if (fileTransferSize != null)
            {
                if (fileTransferSize != fileTransmittedBytes)
                {
                    Logger.Error("WARNING: Declared transfer file size ({0:N0}) differs from the transferred bytes count ({1:N0})", fileTransferSize.Value, fileTransmittedBytes);
                }
            }

            double kBs = 0;
            if (Watch.ElapsedMilliseconds > 0)
                kBs = fileTransmittedBytes/1.024D/Watch.ElapsedMilliseconds;
            Logger.Info($"Elapsed time: {Watch.FormattedElapsed()} - Average rate: {kBs.ToString("N02")} KB/s");
        }

        #endregion
    }
}
