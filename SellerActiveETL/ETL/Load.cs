﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using NLog;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;
using SellerActiveETL.Services;

namespace SellerActiveETL.ETL
{
    public class Load
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _watch = new Stopwatch();

        /// <summary>
        /// This will Update or Insert (if required) each item in the items list. 
        /// </summary>
        /// <param name="etlJob">The current EtlJob being processed</param>
        /// <param name="items">The items that will be Updated/Inserted</param>
        public void Execute(EtlJob etlJob, List<InventoryItemWrapper> items)
        {
            _watch.Reset();
            _watch.Start();
            Logger.Trace("Start");
            etlJob.CurrentStep = Steps.LoadStart;
            var success = 0;
            var fails = 0;
            var inserts = 0;
            var updates = 0;
            var total = items.Count;
            var tenPercent = (int) (total*.10);
            var runningTally = 0;
            var skusInserted = new StringBuilder();
            var skusUpdated = new StringBuilder();
            using (var svc = new InventoryService(etlJob.Client))

            {
                Logger.Info($"Performing Update/Insert on {total:n0} items.");
                foreach (var item in items)
                {
                    if (runningTally++ > tenPercent)
                    {
                        Logger.Info($"{(success+fails):n0}/{total:n0} items processed...");
                        runningTally = 0;
                    }
                    Update(item, etlJob.Client.Source, svc);

                    if (item.UpdateResult.IsSuccess)
                    {
                        Logger.Trace($"Sku {item.InventoryItem.SKU} Updated.");
                        Logger.Trace($"{item.InventoryItem}");
                        success++;
                        updates++;
                        skusUpdated.Append((updates > 1 ? ", " : "") + item.InventoryItem.SKU);
                        continue;
                    }

                    // Only perform an insert on failure if the Status Code was HTTP304 - Not Modified & the client's source indicates to perform an insert on update failure. 
                    if (item.UpdateResult.ErrorCode == HttpStatusCode.NotModified.ToString() && etlJob.Client.Source.PerformInsertOnUpdateFail)
                    {
                        Logger.Trace("Update Failed - attempting insert.");
                        Insert(item, etlJob, svc);

                        if (item.InsertResult.IsSuccess)
                        {
                            Logger.Trace($"Sku {item.InventoryItem.SKU} Inserted.");
                            Logger.Trace($"{item.InventoryItem}");
                            success++;
                            inserts++;
                            skusInserted.Append((inserts > 1 ? ", " : "") + item.InventoryItem.SKU);
                        }
                        else
                        {
                            if (item.InsertResult.ErrorCode == ErrorCode.RuleFailed.ToString())
                                Logger.Debug($"Insert aborted - {item.InsertResult.ErrorMessage}");
                            else 
                                Logger.Warn($"Attempted insert failed. Error Code: {item.InsertResult.ErrorCode} / Message:{item.InsertResult.ErrorMessage}");

                            Logger.Trace(item.InsertResult);
                            fails++;
                        }
                    }
                    else
                    {
                        if (item.UpdateResult.ErrorCode == HttpStatusCode.NotModified.ToString())
                            Logger.Debug($"Update failed - Insert not attempted - HTTP304 - Not Modified - Inventory Not Added, ");
                        else
                            Logger.Warn($"Update failed - Insert not attempted - Error Code: {item.UpdateResult.ErrorCode} / Message:{item.UpdateResult.ErrorMessage}");
                        Logger.Trace(item.InsertResult);
                        fails++;
                    }
                }

            }
            Logger.Debug($"Skus Update: {skusUpdated}");
            Logger.Debug($"Skus Inserted: {skusInserted}");
            Logger.Info($"Load Finished - Total Processed: {fails + success} / Successes: {success} / Failures: {fails} / Updates: {updates} / Inserts {inserts}");
            etlJob.CurrentStep = Steps.LoadEnd;
            _watch.Stop();
            Logger.Info($"Elapsed time for Load: {_watch.FormattedElapsed()}");
            Logger.Trace("End");

        }

        /// <summary>
        /// This will insert any item that passes all RuleType.Insert rules associated with the client.
        /// </summary>
        /// <param name="item">The item that the InsertItem will be based upon</param>
        /// <param name="etlJob"></param>
        /// <param name="svc"></param>
        private void Insert(InventoryItemWrapper item, EtlJob etlJob, InventoryService svc)
        {
            Logger.Trace("Start");
            var itemForInsert = GetInventoryItemForInsert(item.InventoryItem, etlJob.Client.Source);

            Logger.Debug("Processing Pre-Insert Rules.");
            // Process Insert rules
            foreach (var rule in etlJob.Client.Rules.Where(rule => rule.RuleType == RuleType.Insert))
            {
                if (rule.Execute(itemForInsert, etlJob))
                {
                    Logger.Trace($"Rule {rule.RuleName} executed successfully.");
                }
                else
                {
                    item.InsertResult = new Results<InventoryItem>
                    {
                        ErrorCode = ErrorCode.RuleFailed.ToString(),
                        ErrorMessage = $"Rule {rule.RuleName}, failed for SKU: {itemForInsert.SKU}",
                        IsSuccess = false,
                        Item = itemForInsert
                    };
                    return;
                }
            }

            item.InsertResult = svc.CreateAsync(itemForInsert).Result;

            Logger.Trace("End");
        }

        /// <summary>
        /// This will create an item for Update and call the service to update the item.
        /// </summary>
        /// <param name="item">The item which the UpdateItem will be based upon</param>
        /// <param name="source"></param>
        /// <param name="svc"></param>
        private void Update(InventoryItemWrapper item, Source source, InventoryService svc)
        {
            Logger.Trace("Start");
            var updateItem = GetInventoryItemForUpdate(item.InventoryItem, source);

            // TODO: Add Rule Processing for Update Rules -- Will add this when/if there is a need.

            item.UpdateResult = svc.UpdateAsync(updateItem).Result;
            Logger.Trace("End");
        }

        /// <summary>
        /// This will use the Update mappings for the InventoryItem and ProductSites to build a new item for update
        /// </summary>
        /// <param name="item">The item to base the new item from.</param>
        /// <param name="source"></param>
        /// <returns>A new InventoryItem that has the proper fields populated for update</returns>
        public InventoryItem GetInventoryItemForUpdate(InventoryItem item, Source source)
        {
            Logger.Trace("Start");
            var updateItem = new InventoryItem();

            source.UpdateInventoryItemMaps.ForEach(map => SetPropertyValue(updateItem, map, item));
            item.ProductSites?.ForEach(prodSite => source.UpdateProductSiteMaps?.ForEach(map => SetPropertyValue(updateItem.GetProductSite(map.Site), map, prodSite)));

            Logger.Trace("End");

            return updateItem;
        }

        /// <summary>
        /// This will use the Insert mappings for the InventoryItem and ProductSites to build a new item for insert
        /// </summary>
        /// <param name="item">The item to base the new item from.</param>
        /// <param name="source"></param>
        /// <returns>A new InventoryItem that has the proper fields populated for insert</returns>
        public InventoryItem GetInventoryItemForInsert(InventoryItem item, Source source)
        {
            Logger.Trace("Start");
            var updateItem = new InventoryItem();

            source.InsertInventoryItemMaps.ForEach(map => SetPropertyValue(updateItem, map, item));
            item.ProductSites?.ForEach(prodSite => source.InsertProductSiteMaps?.ForEach(map => SetPropertyValue(updateItem.GetProductSite(map.Site), map, prodSite)));

            Logger.Trace("End");

            return updateItem;
        }

        /// <summary>
        /// Sets the value of the property param from the source param based upon the map param.
        /// </summary>
        /// <param name="property">The value of this parameter will be set from the source based upon the map</param>
        /// <param name="map">This map is used to find the property name</param>
        /// <param name="source">Contains a property with the same name of the property param. Map will be used to identify the property by name.</param>
        private static void SetPropertyValue<T>(T property, IMap map, T source)
        {
            Logger.Trace("Start");

            var propertyInfo = property.GetType().GetProperty(map.PropertyName);

            // Get the value from the source and set it to the property
            propertyInfo.SetValue(property, propertyInfo.GetValue(source));
            Logger.Trace("End");
        }
    }
}
