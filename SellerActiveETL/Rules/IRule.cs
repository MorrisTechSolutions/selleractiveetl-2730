using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Rules
{
    public interface IRule
    {
        string RuleName { get;}
        RuleType RuleType { get; }
        bool Execute(InventoryItem item, EtlJob etlJob);
    }
}