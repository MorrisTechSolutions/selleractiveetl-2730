﻿using System;
using System.Linq;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Rules
{
    public class InventoryItemModifyPriceRule : IRule
    {
        private readonly ProductSites _site;
        private readonly double? _defaultPriceModifier;
        private readonly ModifierType? _dpModifierType;
        private readonly double? _floorModifier;
        private readonly ModifierType? _floorModifierType;
        private readonly double? _ceilingModifier;
        private readonly ModifierType? _ceilingModifierType;

        public string RuleName
        {
            get
            {
                var value = "Inventory InventoryItem - Modify Price: ";
                if (_defaultPriceModifier.HasValue)
                    value += $"[Default Price {_dpModifierType}  {_defaultPriceModifier}] ";

                if (_ceilingModifier.HasValue)
                    value += $"[Ceiling {_ceilingModifierType}  {_ceilingModifier}] ";

                if (_floorModifier.HasValue)
                    value += $"[Floor {_floorModifierType}  {_floorModifier}] ";
                return value;
            }
        }

        public RuleType RuleType => RuleType.PostTransform;

        public InventoryItemModifyPriceRule(ProductSites site, double? defaultPriceModifier = null, ModifierType? dpModifierType = null, double? floorModifier = null, ModifierType? floorModifierType = null, double? ceilingModifier = null, ModifierType? ceilingModifierType = null)
        {
            _site = site;
            _defaultPriceModifier = defaultPriceModifier;
            _dpModifierType = dpModifierType;
            _floorModifier = floorModifier;
            _floorModifierType = floorModifierType;
            _ceilingModifier = ceilingModifier;
            _ceilingModifierType = ceilingModifierType;
        }

        public bool Execute(InventoryItem item, EtlJob etlJob)
        {
            foreach (var site in item.ProductSites.Where(site => site.Site == _site.ToString()))
            {
                site.DefaultPrice = (_defaultPriceModifier.HasValue) ? Modify(site.DefaultPrice, _dpModifierType, _defaultPriceModifier) : site.DefaultPrice;
                site.CeilingPrice = (_ceilingModifier.HasValue) ? Modify(site.CeilingPrice, _ceilingModifierType, _ceilingModifier) : site.CeilingPrice;
                site.FloorPrice = (_floorModifier.HasValue) ? Modify(site.FloorPrice, _floorModifierType, _floorModifier) : site.FloorPrice;
            }
            return true;
        }

        private double? Modify(double? price, ModifierType? modifierType, double? modifier)
        {
            // BUG: Potentially a problem. x+=y, if x is nullable, then the result will always be null if it's null to begin with. I'm not sure what the expected
            // BUG: value should be - for now I think this is okay, we don't want to modify an unset value.
            switch (modifierType)
            {
                case ModifierType.Add:
                    price += modifier;
                    break;
                case ModifierType.Subtract:
                    price -= modifier;
                    break;
                case ModifierType.Multiply:
                    price *= modifier;
                    break;
                case ModifierType.Divide:
                    if (modifier == 0) throw new ArgumentException("modifier parameter cannot be 0 when modifierType is Divide", nameof(modifier));
                    price /= modifier;
                    break;
                case ModifierType.Replace:
                    price = modifier;
                    break;
                case null:
                    throw new ArgumentNullException(nameof(modifierType), "modifierType cannot be null");
                default:
                    throw new ArgumentOutOfRangeException(nameof(modifierType), modifierType, "Unexpected value");
            }
            return price;
        }
    }

    public enum ModifierType
    {
        Add,
        Subtract,
        Divide,
        Multiply,
        Replace
    }
}
