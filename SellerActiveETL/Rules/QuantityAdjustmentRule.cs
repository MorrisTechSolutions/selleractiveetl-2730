using System;
using NLog;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Rules
{
    /// <summary>
    /// Will Modify the Quantity by the 'Modifier' amount based upon the ModifierType set. If the Quantity property is null then it will remain null. 
    /// </summary>
    public class QuantityAdjustmentRule : IRule
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string RuleName => $"Modify Quantity: {_modifierType} with {_modifier} if the passed in criteria resolves true";
        
        public RuleType RuleType => RuleType.PostTransform;

        private readonly Func<InventoryItem, bool> _criteria;
        private readonly ModifierType _modifierType;
        private readonly int _modifier;

        public QuantityAdjustmentRule(Func<InventoryItem, bool> criteria, ModifierType modifierType, int modifier)
        {
            Logger.Trace("Start");

            if (criteria == null) throw new ArgumentNullException(nameof(criteria));
            if (modifier == 0 && modifierType == ModifierType.Divide) throw new ArgumentException("_modifier parameter cannot be 0 when _modifierType is Divide", nameof(_modifier));
            
            
            _criteria = criteria;
            _modifierType = modifierType;
            _modifier = modifier;

            Logger.Debug($"{RuleName} has been configured.");

            Logger.Trace("End");
        }

        /// <summary>
        /// Will adjust the Quantity field of the InventoryItem by the set value as per the ModifierType.  
        /// </summary>
        /// <param name="item">The item that will be modified</param>
        /// <param name="etlJob">Unnecessary</param>
        /// <returns>True if modified, false if not modified</returns>
        public bool Execute(InventoryItem item, EtlJob etlJob = null)
        {
            Logger.Trace("Start");
            if (item == null) throw new ArgumentNullException(nameof(item));

            if (_criteria(item))
            {
                var oldQty = item.Quantity;
                item.Quantity = Modify(item.Quantity);
                Logger.Debug($"SKU {item.SKU} quantity adjusted from {oldQty} to {item.Quantity}");
                return true;
            }

            Logger.Trace($"End - {item.SKU} not adjusted.");
            return false;
        }

        private int? Modify(int? quantity)
        {
            Logger.Trace("Start");
            switch (_modifierType)
            {
                case ModifierType.Add:
                    quantity += _modifier;
                    break;
                case ModifierType.Subtract:
                    quantity -= _modifier;
                    break;
                case ModifierType.Multiply:
                    quantity *= _modifier;
                    break;
                case ModifierType.Divide:
                    quantity /= _modifier;
                    break;
                case ModifierType.Replace:
                    quantity = _modifier;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(_modifierType), _modifierType, "Unexpected modifier");
            }

            Logger.Trace("End");
            return quantity;
        }
    }
}