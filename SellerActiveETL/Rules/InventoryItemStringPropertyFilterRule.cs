using System;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Rules
{
    public class InventoryItemStringPropertyFilterRule : IRule
    {
        private readonly string _propertyName;
        private readonly string _oldValue;
        private readonly string _newValue;
        public string RuleName => String.IsNullOrEmpty(_newValue) ? $"Inventory item Property Filter: Removing {_oldValue} from property {_propertyName}" : $"Inventory item Property Filter: Replace {_oldValue} with {_newValue} for property {_propertyName}";
        public RuleType RuleType => RuleType.PostTransform;

        public InventoryItemStringPropertyFilterRule(string propertyName, string oldValue, string newValue = null)
        {
            _propertyName = propertyName;
            _oldValue = oldValue;
            _newValue = newValue;
        }

        public bool Execute(InventoryItem item, EtlJob etlJob)
        {
            var pi = item.GetType().GetProperty(_propertyName);
            var newValue = ((string)pi.GetValue(item)).Replace(_oldValue, _newValue);
            pi.SetValue(item, newValue);

            return true;
        }
    }
}