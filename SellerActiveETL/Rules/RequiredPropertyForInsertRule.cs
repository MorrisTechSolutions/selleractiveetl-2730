using System;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Rules
{
    public class RequiredPropertyForInsertRule : IRule
    {
        private readonly string _propertyName;
        public string RuleName => $"Insert only if {_propertyName} is not default value.";

        public RuleType RuleType => RuleType.Insert;

        public RequiredPropertyForInsertRule(string propertyName)
        {
            _propertyName = propertyName;
        }

        public bool Execute(InventoryItem item, EtlJob etlJob)
        {
            bool isDefaultValue;
            var pi = item.GetType().GetProperty(_propertyName);
            
            var propValue = pi.GetValue(item);

            if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof (Nullable<>))
            {
                isDefaultValue = propValue == null;
            }
            else if (pi.PropertyType == typeof(string))
            {
                isDefaultValue = String.IsNullOrEmpty(propValue.ToString());
            }
            else
            {
                isDefaultValue = pi.PropertyType.GetDefault()?.Equals(propValue) ?? propValue == null;
            }

            return !isDefaultValue;
        }
    }
}