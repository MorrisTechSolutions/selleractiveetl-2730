﻿namespace SellerActiveETL.ETLJobs
{
    public interface IMap
    {
        string ColumnLetter { get; set; }
        string ColumnNumber { get; set; }
        string PropertyName { get; set; }
        string Value { get; set; }
    }
}