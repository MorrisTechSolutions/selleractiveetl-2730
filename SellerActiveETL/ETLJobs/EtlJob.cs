using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NLog.Fluent;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;
using SellerActiveETL.ETL;

namespace SellerActiveETL.ETLJobs
{
    public class EtlJob
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Extract _extract = new Extract();
        private readonly Transform _transform = new Transform();
        private readonly Load _load = new Load();

        private Steps _currentStep;

        public Status Status { get; set; }

        public Steps CurrentStep
        {
            get { return _currentStep; }
            set
            {
                if (value == _currentStep) return;

                _currentStep = value;
                Logger.Info($"Job Current Step: {_currentStep}");
            }
        }

        public Client Client { get; }


        public string LocalDataFileName { get; set; }


        public EtlJob(Client client)
        {
            Client = client;
        }

        /// <summary>
        /// Run the ETL job
        /// </summary>
        public bool Run()
        {
            
            var isValidResults = IsValid();

            if (!isValidResults.IsSuccess)
            {
                Logger.Info($"ETL Job for Client {Client.ClientId} is not configured correctly: {isValidResults.ErrorCode}");
                Logger.Info($"{isValidResults.ErrorMessage}");
                return false;
            }
            Logger.Info($"ETL Job for Client {Client.ClientId} has started.");
            Status = Status.Running;
            CurrentStep = Steps.Started;

            Logger.Info("------------------ EXTRACT ------------------");
            if (!_extract.Execute(this))
            {
                Logger.Fatal("There was an error during the Extract process. Aborting job.");
                Console.ReadLine();
                return false;
            }

            Logger.Info("------------------ TRANSFORM ------------------");
            var transformResults = _transform.Execute(this);

            if (!transformResults.IsSuccess)
            {
                Logger.Fatal("There was an error performing the Transform.");
                Logger.Fatal($"{transformResults.ErrorCode}:{transformResults.ErrorMessage}");
                return false;
            }


            Logger.Info("------------------ LOAD ------------------");
#if DEBUG
            if (Constants.TESTING_RECORDS_TO_SKIP > 0)
            {
                Logger.Info($"TESTING MODE: Skipping the first {Constants.TESTING_RECORDS_TO_SKIP} items");
                transformResults.Item = transformResults.Item.Skip(Constants.TESTING_RECORDS_TO_SKIP).ToList();
            }
            if (Constants.TESTING_RECORDS_TO_TAKE > 0)
            {
                Logger.Info($"TESTING MODE: Taking only the next {Constants.TESTING_RECORDS_TO_TAKE} items");
                transformResults.Item = transformResults.Item.Take(Constants.TESTING_RECORDS_TO_TAKE).ToList();
            }
#endif
            _load.Execute(this, transformResults.Item);

            return true;
        }

        private Results<EtlJob> IsValid()
        {
            InventoryItem item;
            var results = new Results<EtlJob> {IsSuccess = true, Item = this};

            // Make sure SKU is in the mappings - it's a required field.
            if (!Client.Source.UpdateInventoryItemMaps.IsAny(map => map.PropertyName.Equals(nameof(item.SKU))))
            {
                results.IsSuccess = false;
                results.ErrorMessage += "The Source must contain update mappings for SKU\n";
            }

            if (Client.Source.InsertInventoryItemMaps.HasAny() && !Client.Source.UpdateInventoryItemMaps.IsAny(map => map.PropertyName.Equals(nameof(item.SKU))))
            {
                results.IsSuccess = false;
                results.ErrorMessage += "The Source must contain insert mappings for SKU\n";
            }



            // If there were any errors, set the error code.
            if (!results.IsSuccess)
                results.ErrorCode = "Job Invalid";

            return results;
        }


    }
}