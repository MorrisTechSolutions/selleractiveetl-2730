using System;
using System.Collections.Generic;
using SellerActiveETL.Rules;

namespace SellerActiveETL.ETLJobs
{
    public class Client : IEquatable<Client>
    {
        public string ClientId { get; set; }

        public string ApiKey { get; set; }

        public Source Source { get; set; }

        public string Schedule { get; set; }

        public List<IRule> Rules { get; set; }

        public Client(string clientId, string apiKey, Source source, List<IRule> rules) : this(clientId, apiKey,source)
        {
            Rules = rules;
        }

        public Client(string clientId, string apiKey, Source source)
        {
            if (clientId == null) throw new ArgumentNullException(nameof(clientId));
            if (apiKey == null) throw new ArgumentNullException(nameof(apiKey));
            if (source == null) throw new ArgumentNullException(nameof(source));

            ClientId = clientId;
            ApiKey = apiKey;
            Source = source;
        }

        public bool HasRule(string ruleName)
        {
            return Rules.IsAny(rule => rule.RuleName == ruleName);
        }

        public bool Equals(Client other)
        {
            return ClientId.Equals(other.ClientId);
        }
    }
}