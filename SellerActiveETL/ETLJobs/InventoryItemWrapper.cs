﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SellerActiveETL.DTO;

namespace SellerActiveETL.ETLJobs
{
    public class InventoryItemWrapper
    {
        public InventoryItem InventoryItem { get; private set; }

        public InventoryItemWrapper(InventoryItem inventoryItem)
        {
            InventoryItem = inventoryItem;
        }

        public Results<InventoryItem> UpdateResult { get; set; }

        public Results<InventoryItem> InsertResult { get; set; }
    }
}
