﻿using System;
using System.Collections.Generic;
using SellerActiveETL.Enums;

namespace SellerActiveETL.ETLJobs
{
    public class Source
    {
        public string Endpoint { get; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public FileType FileType { get;  }

        public SourceType SourceType { get; }

        public string FileLocation { get; set; }

        public string SourceFileName { get; set; }

        public bool IsZip { get; set; }

        public bool HasHeader { get; }

        public string ZipInternalFileName { get; set; }

        public List<InventoryItemMap> UpdateInventoryItemMaps { get; }

        public List<ProductSiteMap> UpdateProductSiteMaps { get; set; }

        public List<InventoryItemMap> InsertInventoryItemMaps { get; set; }

        public List<ProductSiteMap> InsertProductSiteMaps { get; set; }

        public bool PerformInsertOnUpdateFail => InsertInventoryItemMaps.HasAny();

        public Source(string endPoint, FileType fileType, SourceType sourceType, bool hasHeader, List<InventoryItemMap> updateInventoryItemMaps )
        {
            if (endPoint == null) throw new ArgumentNullException(nameof(endPoint));
            if (updateInventoryItemMaps == null) throw new ArgumentNullException(nameof(updateInventoryItemMaps));
            Endpoint = endPoint;
            FileType = fileType;
            SourceType = sourceType;
            HasHeader = hasHeader;
            UpdateInventoryItemMaps = updateInventoryItemMaps;
        }
    }

}