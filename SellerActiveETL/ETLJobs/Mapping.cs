﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SellerActiveETL.DTO;
using SellerActiveETL.Enums;

namespace SellerActiveETL.ETLJobs
{
    public class InventoryItemMap : IMap
    {
        public string PropertyName { get; set; }
        public string ColumnNumber { get; set; }
        public string ColumnLetter { get; set; }
        public string Value { get; set; }
    }

    public class ProductSiteMap : IMap
    {
        public ProductSites Site { get; set; }
        public string PropertyName { get; set; }
        public string ColumnNumber { get; set; }
        public string ColumnLetter { get; set; }
        public string Value { get; set; }
    }
}
