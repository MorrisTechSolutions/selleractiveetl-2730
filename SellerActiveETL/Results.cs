﻿namespace SellerActiveETL
{
    public class Results<T> 
    {
        public T Item { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
    }
}
