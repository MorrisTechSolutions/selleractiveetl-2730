namespace SellerActiveETL.Enums
{
    public enum Status
    {
        Unknown,
        Scheduled,
        Running,
        Finished
    }
}