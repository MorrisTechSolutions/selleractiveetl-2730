namespace SellerActiveETL.Enums
{
    public enum Steps
    {
        Started,
        ExtractStart,
        ExtractEnd,
        TransformStart,
        TransformProcessingRules,
        TransformEnd,
        LoadStart,
        LoadEnd,
        Finished
    }
}