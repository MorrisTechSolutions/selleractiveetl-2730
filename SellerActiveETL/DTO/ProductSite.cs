using Newtonsoft.Json;

namespace SellerActiveETL.DTO
{
    public class ProductSite
    {
        [JsonConverter(typeof(RoundingJsonConverter))]
        public double? FloorPrice { get; set; }

        [JsonConverter(typeof(RoundingJsonConverter))]
        public double? CeilingPrice { get; set; }

        [JsonConverter(typeof(RoundingJsonConverter))]
        public double? DefaultPrice { get; set; }

        public string Site { get; set; }

        [JsonConverter(typeof(RoundingJsonConverter))]
        public double? Price { get; set; }

        public int? Quantity { get; set; }

        public string CurrencyISO { get; set; }

        public string GroupName { get; set; }
    }
}