using System.Collections.Generic;
using System.Linq;
using SellerActiveETL.Enums;

namespace SellerActiveETL.DTO
{
    public class InventoryItem
    {
        // ReSharper disable once InconsistentNaming
        public string SKU { get; set; }
        public string Condition { get; set; }
        public string Title { get; set; }
        // ReSharper disable once InconsistentNaming
        public string ProductID { get; set; }
        public string ProductType { get; set; }
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public string Description { get; set; }
        public string ConditionNote { get; set; }
        public double? MAPPrice { get; set; }
        public double? ItemWeight { get; set; }
        public double? ItemHeight { get; set; }
        public double? ItemLength { get; set; }
        public double? ItemWidth { get; set; }
        public string ImageURLSmall { get; set; }
        public string ImageURLBig { get; set; }
        public bool? Active { get; set; }
        public bool? IsFBA { get; set; }
        public double? Cost { get; set; }
        public string ModelNumber { get; set; }
        public string Brand { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public string Attribute6 { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string VendorName { get; set; }
        public string COO { get; set; }
        public string HTS { get; set; }
        public string UPC { get; set; }
        public List<ProductSite> ProductSites { get; set; }

        /// <summary>
        /// Will return an existing Product Site that matches the Site passed in or create a new one and add it to the ProductSites list.
        /// </summary>
        /// <param name="site"></param>
        /// <returns>Exisiting or new ProductSite</returns>
        public ProductSite GetProductSite(ProductSites site)
        {
            if (ProductSites.HasAny() && ProductSites.Any(ps => ps.Site == site.ToString()))
                return ProductSites.Single(ps => ps.Site == site.ToString());

            if (ProductSites == null) ProductSites = new List<ProductSite>();
            var newProductSite = new ProductSite {Site = site.ToString()};
            ProductSites.Add(newProductSite);

            return newProductSite;

        }

        public override string ToString()
        {
            var returnValue = $"SKU: {SKU}, VendorName: {VendorName}, Qty: {Quantity}, ProdId: {ProductID}, ProdTyp: {ProductType}, Title: {Title}\n";
            if (ProductSites.HasAny())
                returnValue = ProductSites.Aggregate(returnValue, (current, ps) => current + $"     ProductSite: {ps.Site}, Default Price: {ps.DefaultPrice}, Floor: {ps.FloorPrice}, Ceiling: {ps.CeilingPrice}\n");

            return returnValue;
        }
    }
}