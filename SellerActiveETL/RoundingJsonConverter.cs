﻿using System;
using Newtonsoft.Json;

namespace SellerActiveETL
{
    public class RoundingJsonConverter : JsonConverter
    {
        readonly int _precision;
        readonly MidpointRounding _rounding;

        public RoundingJsonConverter() : this(2) // Default to 2 places
        {
        }

        public RoundingJsonConverter(int precision) : this(precision, MidpointRounding.AwayFromZero)
        {
        }

        public RoundingJsonConverter(int precision, MidpointRounding rounding)
        {
            _precision = precision;
            _rounding = rounding;
        }

        public override bool CanRead => false;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(double);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException("Unnecessary because CanRead is false. The type will skip the converter.");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(Math.Round((double)value, _precision, _rounding));
        }
    }
}
