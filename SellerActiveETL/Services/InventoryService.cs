﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SellerActiveETL.DTO;
using SellerActiveETL.ETLJobs;

namespace SellerActiveETL.Services
{

    public class InventoryService : IDisposable
    {

        private const string END_POINT = "http://rest-beta.selleractive.com:80/api/"; // TODO: Read this from config.
        private const string BASE_URI = "Inventory";
        private readonly string _apiKey;
        private readonly string _clientId;
        private string AuthKey => $"{_clientId}:{_apiKey}";


        public InventoryService(Client client)
        {

            if (String.IsNullOrEmpty(client.ApiKey) || String.IsNullOrEmpty(client.ClientId)) throw new ArgumentNullException(nameof(client), "ClientID & ApiKey cannot be null or empty.");

            _apiKey = client.ApiKey;
            _clientId = client.ClientId;
        }

        private HttpClient _client;
        private HttpClient Client
        {
            get
            {
                if (_client != null) return _client;
                _client = new HttpClient { BaseAddress = new Uri(END_POINT) };
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AuthKey);
                return _client;
            }
        }

        public async Task<Results<InventoryItem>> CreateAsync(InventoryItem item)
        {

            var resp = await Client.PostAsync(BASE_URI, InventoryItemToStringContent(item));
            var results = GetResults<InventoryItem>(resp, r => r.Item = item);

            return results;
        }

        public async Task<Results<List<InventoryItem>>> RetrieveAsync(string sku)
        {
            var resp = await Client.GetAsync($"{BASE_URI}?req.sku={sku}");
            var results = GetResults<List<InventoryItem>>(resp, r => r.Item = JsonConvert.DeserializeObject<List<InventoryItem>>(resp.Content.ReadAsStringAsync().Result));

            if (results.Item.Count != 0) return results;

            results.IsSuccess = false;
            results.Item = null;
            results.ErrorCode = "No Item";
            results.ErrorMessage = $"The service returned no items matching sku: {sku}.";
            return results;
        }

        public async Task<Results<InventoryItem>> UpdateAsync(InventoryItem item)
        {
            var resp = await Client.PutAsync(BASE_URI, InventoryItemToStringContent(item));
            var results = GetResults<InventoryItem>(resp, r => r.Item = item);
            return results;
        }

        public async Task<Results<string>> DeleteAsync(string sku)
        {
            var resp = await Client.DeleteAsync($"{BASE_URI}?req.sku={sku}");
            var results = GetResults<string>(resp, r => r.Item = sku);
            return results;
        }

        private static Results<T> GetResults<T>(HttpResponseMessage resp, Action<Results<T>> onSuccessAction)
        {
            var results = new Results<T> { IsSuccess = resp.IsSuccessStatusCode };
            if (results.IsSuccess)
            {
                onSuccessAction(results);

                return results;
            }

            results.ErrorCode = resp.StatusCode.ToString();
            results.ErrorMessage = resp.ReasonPhrase;
            return results;
        }

        private static StringContent InventoryItemToStringContent(InventoryItem item)
        {
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, FloatFormatHandling = FloatFormatHandling.String };
            var json = JsonConvert.SerializeObject(item, Formatting.Indented, settings);
            //Console.WriteLine(json);
            return new StringContent(json, Encoding.UTF8, "application/json");

        }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }
}
