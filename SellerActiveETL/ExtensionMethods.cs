using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SellerActiveETL
{
    public static class ExtensionMethods
    {
        public static bool HasAny<T>(this IEnumerable<T> enumerable)
        {
            return enumerable != null && enumerable.Any();
        }

        public static bool IsAny<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate )
        {
            return enumerable != null && enumerable.Any(predicate);
        }

        public static bool HasValue(this String str)
        {
            return String.IsNullOrEmpty(str) == false;
        }

        public static int ColumnNameToIndex(this string columnName)
        {
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");

            columnName = columnName.ToUpperInvariant();

            var sum = 0;

            for (var i = 0; i < columnName.Length; i++)
            {
                sum *= 26;
                sum += (columnName[i] - 'A');
            }

            return sum;
        }

        /// <summary>
        /// Will return an instance of a value type otherwise will return null
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetDefault(this Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        public static string FormattedElapsed(this Stopwatch stopwatch)
        {
            var ts = stopwatch.Elapsed;
            return $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds/10D:00}";
        }
    }
}